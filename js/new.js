const root = document.querySelector("#root");

// TASK 1

console.log(`Завдання №1`);

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const all = [];

[...clients1, ...clients2].forEach((el) => {
  if(all.includes(el)) {
    return
  }
    all.push(el);
});

console.log(all);

// TASK 2

console.log(`Завдання №2`);


const characters = [
    {
      name: "Елена",
      lastName: "Гилберт",
      age: 17, 
      gender: "woman",
      status: "human"
    },
    {
      name: "Кэролайн",
      lastName: "Форбс",
      age: 17,
      gender: "woman",
      status: "human"
    },
    {
      name: "Аларик",
      lastName: "Зальцман",
      age: 31,
      gender: "man",
      status: "human"
    },
    {
      name: "Дэймон",
      lastName: "Сальваторе",
      age: 156,
      gender: "man",
      status: "vampire"
    },
    {
      name: "Ребекка",
      lastName: "Майклсон",
      age: 1089,
      gender: "woman",
      status: "vempire"
    },
    {
      name: "Клаус",
      lastName: "Майклсон",
      age: 1093,
      gender: "man",
      status: "vampire"
    }
];

const charactersShortInfo = characters.map((el) => {
  return {name: el.name, lastName: el.lastName, age: el.age}
});

console.log(charactersShortInfo);



// TASK 3

console.log(`Завдання №3`);

const user1 = {
    name: "John",
    years: 30
};

class User {
  constructor({name = none, years = 0, isAdmin = false}) {
    this.name = name;
    this.years = years;
    this.isAdmin = isAdmin
  }

  render(root) {
    const paragr1 = document.createElement("p");
    const paragr2 = document.createElement("p");
    const paragr3 = document.createElement("p");
    paragr1.textContent = `Нова змінна name - ${this.name}`;
    paragr2.textContent = `Нова змінна years - ${this.years}`;
    paragr3.textContent = `Нова змінна years - ${this.isAdmin}`;

  
    root.append(paragr1);
    root.append(paragr2);
    root.append(paragr3);
  }
}

const user = new User(user1);
user.render(root);
console.log(user);


// TASK 4

console.log(`Завдання №4`);

const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
      lat: 38.869422, 
      lng: 139.876632
    }
};
  
const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
};
  
const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto', 
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
};

const fullProfile = {
  ...satoshi2018,
  ...satoshi2019,
  ...satoshi2020
}

console.log(fullProfile);
  

// TASK 5

console.log(`Завдання №5`);

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

const allBooks = [...books, bookToAdd];
console.log(allBooks);


// TASK 6

console.log(`Завдання №6`);

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

class Employee {
  constructor({name, surname}, age, salary) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.salary = salary;
  }
}

// Варіант №1

const newEmployee = new Employee(employee, 55, `3000 $`);

// Варіант №2

// const newEmployee = {...employee, age: 55, sallary: `3000 $`};


console.log(newEmployee);


// TASK 7

console.log(`Завдання №7`);

const array = ['value', () => 'showValue'];

// Допишіть код тут

const [value, showValue] = array;

console.log(value);
console.log(showValue());

alert(value); // має бути виведено 'value'
alert(showValue());  // має бути виведено 'showValue'